package com.atlassian.jira.rpc.soap.service;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.rpc.exception.RemoteException;
import com.atlassian.jira.rpc.exception.RemotePermissionException;
import com.atlassian.jira.rpc.soap.beans.RemotePermission;
import com.atlassian.jira.rpc.soap.beans.RemotePermissionScheme;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

/**
 * A simple helper class that SchemeService and ProejctService can use to populate a RemotePermissionScheme.
 */
public interface ServiceHelper
{
    RemotePermissionScheme populateSchemePermissions(User admin, GenericValue permissionScheme) throws RemoteException, GenericEntityException;

    RemotePermission[] getAllPermissions(User admin) throws RemotePermissionException, RemoteException;

    String getPermissionName(Integer id);
}
