package com.atlassian.jira.rpc.soap.service;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.permission.Permission;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.SchemePermissions;
import com.atlassian.jira.rpc.exception.RemoteException;
import com.atlassian.jira.rpc.exception.RemotePermissionException;
import com.atlassian.jira.rpc.soap.beans.RemoteEntity;
import com.atlassian.jira.rpc.soap.beans.RemotePermission;
import com.atlassian.jira.rpc.soap.beans.RemotePermissionScheme;
import com.atlassian.jira.rpc.soap.util.RemoteEntityFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.annotations.VisibleForTesting;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;
import java.util.Map;

public class ServiceHelperImpl implements ServiceHelper
{
    private static final Logger log = Logger.getLogger(ServiceHelperImpl.class);
    
    private final PermissionManager permissionManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final RemoteEntityFactory remoteEntityFactory;
    private final UserManager userManager;

    public ServiceHelperImpl(final PermissionManager permissionManager,
                             final PermissionSchemeManager permissionSchemeManager,
                             final RemoteEntityFactory remoteEntityFactory,
                             final UserManager userManager)
    {
        this.permissionManager = permissionManager;
        this.permissionSchemeManager = permissionSchemeManager;
        this.remoteEntityFactory = remoteEntityFactory;
        this.userManager = userManager;
    }

    public RemotePermissionScheme populateSchemePermissions(User admin, GenericValue permissionScheme) throws RemoteException, GenericEntityException
    {
        RemotePermissionScheme remotePermissionScheme;
        //iterate over all permissions to create return value
        RemotePermission[] allPermissions = this.getAllPermissions(admin);
        remotePermissionScheme = new RemotePermissionScheme(permissionScheme);
        for (int i = 0; i < allPermissions.length; i++)
        {
            RemotePermission remotePermission = allPermissions[i];
            List entityMappings = permissionSchemeManager.getEntities(permissionScheme, remotePermission.getPermission());
            this.populatePermissionEntityMappings(entityMappings, remotePermissionScheme, remotePermission);
        }
        return remotePermissionScheme;
    }

    protected void populatePermissionEntityMappings(List entityMappings, RemotePermissionScheme remotePermissionScheme, RemotePermission permission)
    {
        //add entity mappings for this permission if there are.
        if (entityMappings.size() > 0)
        {
            RemoteEntity[] remoteEntities = new RemoteEntity[entityMappings.size()];
            for (int j = 0; j < remoteEntities.length; j++)
            {
                GenericValue entityMapping = (GenericValue) entityMappings.get(j);
                if ("group".equals(entityMapping.getString("type")))
                {
                    Group group = userManager.getGroup(entityMapping.getString("parameter"));
                    if (group != null)
                    {
                        remoteEntities[j] = remoteEntityFactory.createGroup(group);
                    }
                    else
                    {
                        remoteEntities[j] = null;
                        log.info("Group permission mapping for: " + permission.getName() + " is allowed for Anyone which is represented by a null group.");
                    }
                }
                if ("user".equals(entityMapping.getString("type")))
                {
                    User user = userManager.getUser(entityMapping.getString("parameter"));
                    if (user != null)
                    {
                        remoteEntities[j] = remoteEntityFactory.createUser(user);
                    }
                    else
                    {
                        remoteEntities[j] = null;
                        log.info("User permission mapping for: " + permission.getName() + " is being represented by a null User.");
                    }
                }
            }
            remotePermissionScheme.addPermissionMapping(permission, remoteEntities);
        }
    }

    public RemotePermission[] getAllPermissions(User admin) throws RemotePermissionException, RemoteException
    {
        if (!(permissionManager.hasPermission(Permissions.ADMINISTER, admin)))
        {
            throw new RemotePermissionException("You do not have permission to get permissions.");
        }

        Map permissions = getSchemePermissions().getSchemePermissions();
        int size = permissions.size();
        Object[] keys = permissions.keySet().toArray();
        Object[] values = permissions.values().toArray();
        RemotePermission[] remotePermissions = new RemotePermission[size];
        for (int i = 0; i < size; i++)
        {
            remotePermissions[i] = new RemotePermission(Long.valueOf("" + keys[i]), ((Permission) values[i]).getName());
        }

        return remotePermissions;
    }

    public String getPermissionName(final Integer id)
    {
        return getSchemePermissions().getPermissionName(id);
    }

    @VisibleForTesting
    SchemePermissions getSchemePermissions()
    {
        return ComponentAccessor.getComponentOfType(SchemePermissions.class);
    }

}
