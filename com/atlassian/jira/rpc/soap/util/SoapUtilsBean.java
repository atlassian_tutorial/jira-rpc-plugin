package com.atlassian.jira.rpc.soap.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.rpc.exception.RemoteException;
import com.atlassian.jira.rpc.soap.beans.RemoteComment;
import com.atlassian.jira.rpc.soap.beans.RemoteField;
import com.atlassian.jira.rpc.soap.beans.RemoteFieldValue;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.Map;

public interface SoapUtilsBean
{
    /**
     * @deprecated See {@link #isVisible(com.atlassian.jira.issue.Issue, String)} for usage instructions instead.
     * @param issue
     * @param fieldname
     * @return
     * @throws com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException
     * @deprecated Please use {@link #isVisible(com.atlassian.jira.issue.Issue, String)} instead.
     */
    @Deprecated
    boolean isVisible(GenericValue issue, String fieldname) throws FieldLayoutStorageException;

    /**
     * @deprecated Please use {@link com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager#getFieldLayout(com.atlassian.jira.issue.Issue)}
     * to obtain the {@link com.atlassian.jira.issue.fields.layout.field.FieldLayout} for the specified issue, then
     * use {@link com.atlassian.jira.issue.fields.layout.field.FieldLayout#isFieldHidden(String)} with the id of the field.
     */
    @Deprecated
    boolean isVisible(Issue issue, String fieldname) throws FieldLayoutStorageException;

    /**
     * @deprecated Use {@link com.atlassian.jira.rpc.soap.beans.ExternalComments#createFrom(com.atlassian.jira.rpc.soap.beans.RemoteComment)} instead.
     */
    @Deprecated
    ExternalComment mapRemoteCommentToExternalComment(RemoteComment remoteComment);

    /**
     * @deprecated Use {@link com.atlassian.jira.rpc.soap.beans.RemoteFields#createFrom(Iterable)} instead.
     */
    @Deprecated
    RemoteField[] convertFieldsToRemoteFields(Collection fields);

    /**
     * @deprecated Use {@link com.atlassian.jira.rpc.soap.beans.RemoteFieldValues#asActionParams(com.atlassian.jira.rpc.soap.beans.RemoteFieldValue[])} instead.
     */
    @Deprecated
    Map mapFieldValueToMap(RemoteFieldValue[] actionParams);

    /**
     * @deprecated Use {@link com.atlassian.jira.bc.issue.IssueService#update(com.atlassian.crowd.embedded.api.User, com.atlassian.jira.bc.issue.IssueService.UpdateValidationResult)} instead. Since v5.0.
     */
    @Deprecated
    void updateIssue(MutableIssue issueObject,
                     OperationContext operationContext,
                     User user,
                     ErrorCollection errors,
                     I18nHelper i18n) throws RemoteException;

    /**
     * @deprecated Use {@link com.atlassian.jira.bc.issue.IssueService#validateUpdate(com.atlassian.crowd.embedded.api.User, Long, com.atlassian.jira.issue.IssueInputParameters)} instead. Since v5.0.
     */
    @Deprecated
    void validate(Issue issueObject,
                  OperationContext operationContext,
                  Map actionParams,
                  User user,
                  ErrorCollection errors,
                  I18nHelper i18n) throws RemoteException;

    /**
     * @deprecated Use {@link com.atlassian.jira.web.action.issue.UpdateFieldsHelperBean#getFieldsForEdit(com.atlassian.crowd.embedded.api.User, com.atlassian.jira.issue.Issue)} and
     * {@link com.atlassian.jira.rpc.soap.beans.RemoteFields#createFrom(Iterable)} instead.
     */
    @Deprecated
    RemoteField[] getFieldsForEdit(User user, Issue issueObject);

    /**
     * @deprecated Use {@link com.atlassian.jira.web.action.issue.IssueCreationHelperBean#getFieldsForCreate(com.atlassian.crowd.embedded.api.User, com.atlassian.jira.issue.Issue)} and
     * {@link com.atlassian.jira.rpc.soap.beans.RemoteFields#createFrom(Iterable)} instead.
     */
    @Deprecated
    RemoteField[] getFieldsForCreate(User user, Issue issue);

    /**
     * This will set the user into the JIRA authentication context (ThreadLocal) and return the prevous user who was in
     * there.  Make sure you use a try / finally block when calling this method
     *
     * @deprecated set the user using {@link com.atlassian.jira.security.JiraAuthenticationContext#setLoggedInUser(com.atlassian.crowd.embedded.api.User)}
     * instead
     *
     * @param user the user to set into the JIRA auth context
     * @return the previous user that was in the JIRA auth context
     */
    @Deprecated
    User setRemoteUserInJira(User user);
}
