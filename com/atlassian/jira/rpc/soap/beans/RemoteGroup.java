package com.atlassian.jira.rpc.soap.beans;

import com.atlassian.crowd.embedded.api.Group;

/**
 * RemoteGroup
 */
public class RemoteGroup extends RemoteEntity
{
    protected RemoteUser[] users;

    public RemoteGroup()
    {
        //do nothing
    }

    public RemoteUser[] getUsers()
    {
        return users;
    }

    public void setUsers(RemoteUser[] users)
    {
        this.users = users;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
