package com.atlassian.jira.rpc.soap.beans;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class RemoteFieldValues
{
    private RemoteFieldValues()
    {
    }

    /**
     * Converts an array of {@link RemoteFieldValue} instances to their equivalent
     * {@link Map} shaped actionParams.
     *
     * @param actionParams the {@link RemoteFieldValue} instances to convert. Should not be null.
     * @return the map containing actionParams.
     */
    public static Map<String, String[]> asActionParams(final RemoteFieldValue[] actionParams)
    {
        checkNotNull(actionParams);

        final Map<String, String[]> map = new HashMap<String, String[]>(actionParams.length);
        for (final RemoteFieldValue remoteFieldValue : actionParams)
        {
            map.put(remoteFieldValue.getId(), remoteFieldValue.getValues());
        }
        return map;

    }
}
