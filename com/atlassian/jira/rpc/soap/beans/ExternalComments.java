package com.atlassian.jira.rpc.soap.beans;

import com.atlassian.jira.external.beans.ExternalComment;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

public class ExternalComments
{
    private static final Logger log = Logger.getLogger(ExternalComments.class);

    private ExternalComments()
    {
    }

    /**
     * Creates an {@link ExternalComment} based on the contents of the passed in {@link RemoteComment}.
     *
     * @param remoteComment the {@link RemoteComment} used for creation. Should not be null.
     * @return the created {@link ExternalComment} instance.
     */
    public static ExternalComment createFrom(final RemoteComment remoteComment)
    {
        checkNotNull(remoteComment, "Remote comment should not be null");

        final ExternalComment comment = new ExternalComment();
        try
        {
            PropertyUtils.copyProperties(comment, remoteComment);
        }
        catch (Exception e)
        {
            log.error("Unable to create external comments from the remote comment");
            throw new RuntimeException(e);
        }
        return comment;
    }
}
