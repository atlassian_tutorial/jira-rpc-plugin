/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

/*
 */
package com.atlassian.jira.rpc.soap.beans;

import org.ofbiz.core.entity.GenericValue;

public class RemoteStatus extends AbstractRemoteConstant
{
    ///CLOVER:OFF
    public RemoteStatus()
    {
    }

    public RemoteStatus(GenericValue gv)
    {
        super(gv);
    }
}
