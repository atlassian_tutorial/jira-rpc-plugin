package com.atlassian.jira.rpc.soap.beans;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.util.concurrent.Nullable;
import com.google.common.base.Function;

import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Iterables.transform;

public class RemoteFields
{
    private RemoteFields()
    {
    }

    /**
     * Converts {@link Field} instances to {@link RemoteField} ones.
     * @param fields {@link Field} instances to convert
     * @return equivalent {@link RemoteField} instances.
     */
    public static RemoteField[] createFrom(final Iterable<Field> fields)
    {
        return toArray
            (
                transform(fields, new Function<Field, RemoteField>()
                {
                    public RemoteField apply(@Nullable final Field field)
                    {
                        return new RemoteField(field.getId(), field.getName());
                    }
                }),
                RemoteField.class
            );
    }
}
